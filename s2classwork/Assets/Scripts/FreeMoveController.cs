using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeMoveController : MonoBehaviour
{
    public float speed = 5.0f;                  //controls the character's speed

    private Vector3 input;                      //tracks pressed inputs as numbers
    private Vector3 motion;                     //tracks movement step per frame
    private CharacterController controller;

    private void Awake()
    {
        //Make reference to the character controller component attached to this object
        controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        //reset the motion to zero
        motion = Vector3.zero;

        input = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Depth"), Input.GetAxis("Vertical"));

        //calculating local transforms
        motion += transform.forward.normalized * input.z;
        motion += transform.right.normalized * input.x;

        //calculating the global tranform of the fish for up and down movement
        motion += Vector3.up.normalized * input.y;

        //Apply the movement to the charcater
        controller.Move(motion * speed * Time.deltaTime);
    }
}
