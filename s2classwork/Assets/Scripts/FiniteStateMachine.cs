using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]   //when script is dragged to the object it auomatically adds the NavMesgAgent component to the object as well
public class FiniteStateMachine : MonoBehaviour
{
    public IdleState idleState;
    public ChaseState chaseState;
    public float lookRadius = 10f;

    private BehaviourState currentState;

    public NavMeshAgent Agent { get; private set; }
    public Transform Target { get; private set; } 

    private void Awake()
    {
        //Instances of the states are created in this awake function
        //Seeting the dinner table
        // the keyword this makes sure the current instance of the object is passed
        idleState = new IdleState(this);
        chaseState = new ChaseState(this);
        Agent = GetComponent<NavMeshAgent>(); // get the reference to the component teh agent will be using
    }

    private void Start()
    {
        //getting ready to eat
        SetState(idleState);
    }

    /// <summary>
    /// Exit current state by running exit function and enter the new state and call its enter function
    /// </summary>
    /// <param name="newState"></param>
    public void SetState(BehaviourState newState)
    {
        // Check if there is a current state with a value (state) (Check to see if a state is already running)
        // If there is a current state, initiate the state exit function
        if(currentState != null)
        {
            currentState.OnStateExit();
        }
        currentState = newState;        // set the current state to the new state that was teh argument to this function
        currentState.OnStateEnter();    // Call the enter state function for the new state
    }

    private void Update()
    {
        Target = CheckForPlayer();

        // having the meal
        if(currentState != null)
        {
            currentState.OnStateUpdate();
        }
    }

    private void OnDrawGizmos()
    {
        //Draw stuff in the scene view only not the game play window

        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }

    public Transform CheckForPlayer()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, lookRadius); //stores all colliders that are within the look radius

        foreach (Collider c in colliders)
        {
            if (c.tag == "Player")
            {
                Debug.Log("Player detected.");
                return c.transform;
            }
        }
        return null;
    }
}

public abstract class BehaviourState
{
    // This is an abstract class. It can never be instantiated, only other classes that derive from it

    protected FiniteStateMachine Instance { get; private set; }

    // This is a constructor
    public BehaviourState(FiniteStateMachine instance)
    {
        Instance = instance;
    }
    
    // The following functions are virtual. Their functionalities do not need to be defined here
    // Each state will define its own Enter, Update and Exit
    public virtual void OnStateEnter() { }
    public virtual void OnStateUpdate() { }
    public virtual void OnStateExit() { }
}

//-------------------------------------------------------------------------- Idle State ---------------------------------------------------------
public class IdleState : BehaviourState
{
    // This is a class that inherits from the abstract class Behaviour State
    // Need a constructor for the state. This state has access to BehaviourState

    //This is the constructor. Base class is the class this class is inherting from
    //so in this case it is BehaviorState. base() passes the instace to the BehaviourState
    //Passes FSM instance to the base class
    public IdleState(FiniteStateMachine instance) : base(instance)
    {
        //Debug.Log("Test state A has been constructed.");
    }

    //Overriding the base class methods

    public override void OnStateEnter()
    {
        //base.OnStateEnter(); // This would have run anything in thevirutual class at the top of there was naything there

        Debug.Log("Idle State has been entered. Agnet is stopped.");

        Instance.Agent.isStopped = false;
    }

    public override void OnStateUpdate()
    {
        Debug.Log("Idling...la la la...");

        if(Instance.Target != null)
        {
            Instance.SetState(Instance.chaseState);
        }
    }

    public override void OnStateExit()
    {
        Debug.Log("Idle State has exited.");
    }
}

//-------------------------------------------------------------------------- Chase State ---------------------------------------------------------
public class ChaseState : BehaviourState
{
    private float distance;

    // This is a class that inherits from the abstract class Behaviour State
    // Need a constructor for TestStateB 
    // TestStateA has access to BehaviourState

    //This is the constructor. Base class is the class this class is inherting from
    //so in this case it is BehaviorState. base() passes the instace to the BehaviourState
    //Passes FSM instance to the base class
    public ChaseState(FiniteStateMachine instance) : base(instance)
    {
       // Debug.Log("Chase state has been constructed.");
    }

    //Overriding the base class methods

    public override void OnStateEnter()
    {
        //base.OnStateEnter(); // This would have run anything in thevirutual class at the top of there was naything there

        Debug.Log("Chase State has been entered.");

        //check that there is a target to chase if not go back to idling
        if(Instance.Target == null)
        {
            Instance.SetState(Instance.idleState);
        }

        Instance.Agent.isStopped = false;
    }

    public override void OnStateUpdate()
    {
        Debug.Log("Chasing......");


        if (Instance.Target != null)
        {
            // set distance to distance from AI to target
            distance = Vector3.Distance(Instance.transform.position, Instance.Target.position);

            //check to see if the distance is less than the agent's stopping distance
            if (distance <= Instance.Agent.stoppingDistance)
            {
                //stop moving towards the target
                if (Instance.Agent.isStopped == false)
                {
                    Instance.Agent.isStopped = true;
                }
            }
            else
            {
                //if not stopped continue moving towards taregt
                if (Instance.Agent.isStopped == true)
                {
                    //else unstop agent
                    Instance.Agent.isStopped = false;
                }
                else
                {
                    Vector3 targetPosition = Instance.Target.position;      //create a target position vector
                    targetPosition.y = Instance.transform.position.y;       //ignore the target's y value
                    Instance.Agent.SetDestination(targetPosition);          //move towards target position
                }
            }
        }
        else 
        {
            Instance.SetState(Instance.idleState);
        }
    }

    public override void OnStateExit()
    {
        Debug.Log("Chase state has exited.");
    }
}