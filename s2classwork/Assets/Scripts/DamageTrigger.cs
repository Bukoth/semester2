using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageTrigger : MonoBehaviour
{
    public float damage = 15f;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            //check to see if the object exists first with a reference to a health component
            if(other.TryGetComponent(out Health health) == true)
            {
                if(health.OnDamage(damage) == true)
                {
                    Debug.Log(health.CurrentHealth);
                }
            }
        }
    }
}
