using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonInteraction : MonoBehaviour, IInteraction
{
    private Animator anim;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    //we need to inherit the interaction namescape
    //c# allows multiole inheritace of interfaces
    //this class needs to implemnet the activate methid see below

    public void Activate()
    {
        Debug.Log("Button");
        anim.SetTrigger("pressButton");
    }
}
