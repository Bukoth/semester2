using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public float duration = 3;

    private float timer = -1;

    // Start is called before the first frame update
    void Start()
    {
        timer = duration;    
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0))  //if left mouse is held down
        {
            if (timer > -1)  //if timer is ocunting
            {
                timer += Time.deltaTime;    //increment timer by frame to frame time

                if (timer > duration)   //check if the timer is greater than duration
                {
                    timer = -1; //turn off timer
                    Debug.Log("Mouse down for " + duration + " seconds");
                    //Timer up functionality goes here
                }
            }
            else //if timer is not counting
            {
                timer = 0; //start timer
                Debug.Log("Timer starts ");
            }
        }
        else if (timer > 0) //if mouse is relased and timer is counting
        {
            timer = -1; //turn timer off
            Debug.Log("Mouse button released");
        }
    }
}
