using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    public float maxHealth = 100f;

    public Image healthBar;
    public Text hpText;

    public float CurrentHealth { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        CurrentHealth = maxHealth;
        healthBar.fillAmount = (CurrentHealth / maxHealth);
        hpText.text = maxHealth.ToString();
    }

    public bool OnDamage(float amount)
    {
        if(CurrentHealth > 0)
        {
            CurrentHealth -= amount;

            healthBar.fillAmount = (CurrentHealth/maxHealth);
            hpText.text = CurrentHealth.ToString();

            if (CurrentHealth <= 0)
            {
                CurrentHealth = 0;
                OnDeath();
            }
            return true;
        }
        return false;
    }

    private void OnDeath()
    {
        Debug.Log("Player has died!");
    }
}
